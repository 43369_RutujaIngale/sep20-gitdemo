#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void librarian_area(user_t *u)
{
    int choice,name;
do{
    printf("\n 0.Sign Out\n1.add new member\n2.add new copy\n3.edit profile\n4.edit PW\n5.edit book\n6.find book\n7.add new book\n8.check avaibility\n9.return copy\n10.issue copy\n11.change rack\n12.take paymenr\n13.sign in");
    scanf("%d",&choice);
    switch(choice) 
    {
        case 1:
			add_member();
        	break;

        case 2:
			bookadd();
       	 	break;

        case 3:
		user_edit_by_id();
        break;

        case 4:
        break;

        case 5:
			book_edit_by_id();
        	break;

        case 6: printf("Enter book name: ");
				scanf("%s", name);
				book_find();
        break;
        
        case 7:
        break;
        
        case 8:
			bookcopy_checkavail_details();
        	break;

        case 9:
        break;

        case 10:
			bookcopy_issue();
        	break;

        case 11:
        break;

        case 12:
        break;

        case 13:
			sign_up();
       		 break;

        
    }
}while(choice!=0);
}

void add_member() {
	// input member details
	user_t u;
	user_accept(&u);
	// add librarian into the users file
	user_add(&u);
}
void book_edit_by_id()
 {
	int id, found = 0;
	FILE *fp;
	book_t b;
	// input book id from user.
	printf("enter book id: ");
	scanf("%d", &id);
	// open books file
	fp = fopen(BOOK_DB, "rb+");
	if(fp == NULL) 
    {
		perror("cannot open books file");
		exit(1);
	}
	// read books one by one and check if book with given id is found.
	while(fread(&b, sizeof(book_t), 1, fp) > 0)
        {
		if(id == b.id) {
			found = 1;
			break;
		}
	}
	// if found
	if(found) {
		// input new book details from user
		long size = sizeof(book_t);
		book_t nb;
		book_accept(&nb);
		nb.id = b.id;
		// take file position one record behind.
		fseek(fp, -size, SEEK_CUR);
		// overwrite book details into the file
		fwrite(&nb, size, 1, fp);
		printf("book updated.\n");
	}
	else // if not found
		// show message to user that book not found.
		printf("Book not found.\n");
	// close books file
	fclose(fp);
}

void user_edit_by_id()
 {
	int id, found = 0;
	FILE *fp;
	user_t u;
	// input book id from user.
	printf("enter user id: ");
	scanf("%d", &id);
	// open books file
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) 
    {
		perror("cannot open users file");
		exit(1);
	}
	// read books one by one and check if book with given id is found.
	while(fread(&u, sizeof(user_t), 1, fp) > 0)
        {
		if(id == u.id) {
			found = 1;
			break;
		}
	}
	// if found
	if(found) {
		// input new book details from user
		long size = sizeof(user_t);
		user_t nb;
		user_accept(&nb);
		nb.id = u.id;
		// take file position one record behind.
		fseek(fp, -size, SEEK_CUR);
		// overwrite book details into the file
		fwrite(&nb, size, 1, fp);
		printf("user updated.\n");
	}
	else // if not found
		// show message to user that book not found.
		printf("user not found.\n");
	// close books file
	fclose(fp);
}

void bookcopy_add() {
	FILE *fp;
	// input book copy details
	bookcopy_t b;
	bookcopy_accept(&b);
	b.id = get_next_book_id();
	// add book copy into the books file
	// open book copies file.
	fp = fopen(BOOKCOPY_DB, "ab");
	if(fp == NULL) {
		perror("cannot open book copies file");
		exit(1);
	}
	// append book copy to the file.
	fwrite(&b, sizeof(bookcopy_t), 1, fp);
	printf("book copy added in file.\n");
	// close book copies file.
	fclose(fp);
}

void bookcopy_checkavail_details() {
	int book_id;
	FILE *fp;
	bookcopy_t bc;
	int count = 0;
	// input book id
	printf("enter the book id: ");
	scanf("%d", &book_id);
	// open book copies file
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL) {
		perror("cannot open bookcopies file.");
		return;
	}

	// read bookcopy records one by one
	while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
		// if book id is matching and status is available, print copy details
		if(bc.bookid == book_id && strcmp(bc.status, STATUS_AVAIL)==0) {
			bookcopy_display(&bc);
			count++;
		}
	}
	// close book copies file
	fclose(fp);
	// if no copy is available, print the message. 
	if(count == 0)
		printf("no copies availables.\n");
}


void display_issued_bookcopies(int member_id) {
	FILE *fp;
	issuerecord_t rec;
	// open issue records file
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}

	// read records one by one
	while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) {
		// if member_id is matching and return date is 0, print it.
		if(rec.memberId == member_id && rec.returndate.day == 0)
			issuerecord_display(&rec);
	}
	// close the file
	fclose(fp);
}


void bookcopy_return() {
	int member_id, record_id;
	FILE *fp;
	issuerecord_t rec;
	int diff_days, found = 0;
	long size = sizeof(issuerecord_t);
	// input member id
	printf("enter member id: ");
	scanf("%d", &member_id);
	// print all issued books (not returned yet)
	display_issued_bookcopies(member_id);
	// input issue record id to be returned.
	printf("enter issue record id (to return): ");
	scanf("%d", &record_id);
	// open issuerecord file
	fp = fopen(ISSUERECORD_DB, "rb+");
	if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}
	// read records one by one
	while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) {
		// find issuerecord id
		if(record_id == rec.id) {
			found = 1;
			// initialize return date
			rec.returndate = date_current();
			// check for the fine amount
			diff_days = date_cmp(rec.returndate, rec.return_duedate);
			// update fine amount if any
			if(diff_days > 0)
				rec.fine_amount = diff_days * FINE_PER_DAY;
			break;
		}
	}
	
	if(found) {
		// go one record back
		fseek(fp, -size, SEEK_CUR);
		// overwrite the issue record
		fwrite(&rec, sizeof(issuerecord_t), 1, fp);
		// print updated issue record.
		printf("issue record updated after returning book:\n");
		issuerecord_display(&rec);
		// update copy status to available 
		bookcopy_changestatus(rec.copyid, STATUS_AVAIL);
	}
	
	// close the file.
	fclose(fp);
}


void bookcopy_issue() {
	issuerecord_t rec;
	FILE *fp;
	// accept issuerecord details from user
	issuerecord_accept(&rec);
	//TODO: if user is not paid, give error & return.
	// generate & assign new id for the issuerecord
	rec.id = get_next_issuerecord_id();
	// open issuerecord file
	fp = fopen(ISSUERECORD_DB, "ab");
	if(fp == NULL) {
		perror("issuerecord file cannot be opened");
		exit(1);
	}
	// append record into the file
	fwrite(&rec, sizeof(issuerecord_t), 1, fp);
	// close the file
	fclose(fp);

	// mark the copy as issued
	bookcopy_changestatus(rec.copyid, STATUS_ISSUED);
}

void bookcopy_changestatus(int bookcopy_id, char status[]) {
	bookcopy_t bc;
	FILE *fp;
	long size = sizeof(bookcopy_t);
	// open book copies file
	fp = fopen(BOOKCOPY_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open book copies file");
		return;
	}

	// read book copies one by one
	while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
		// if bookcopy id is matching
		if(bookcopy_id == bc.id) {
			// modify its status
			strcpy(bc.status, status);
			// go one record back
			fseek(fp, -size, SEEK_CUR);
			// overwrite the record into the file
			fwrite(&bc, sizeof(bookcopy_t), 1, fp);
			break;
		}
	}
	
	// close the file
	fclose(fp);
}

