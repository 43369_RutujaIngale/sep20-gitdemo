#ifndef _LIBRARY_H
#define _LIBRARY_H
#include "date.h"

#define USER_DB		    "users.db"
#define BOOK_DB		    "book.db"
#define BOOKCOPY_DB	"bookcopies.db"
#define ISSUERECORD_DB	"issuerecord.db"


#define STATUS_ISSUED	"issued"
#define STATUS_AVAIL	"available"

#define FINE_PER_DAY			5
#define BOOK_RETURN_DAYS		7
#define MEMBERSHIP_MONTH_DAYS	30

#define PAY_TYPE_FEES	"fees"
#define PAY_TYPE_FINE	"fine"


#define EMAIL_OWNER     "bhagu@gmail.com"

#define ROLE_MEMBER        "member"
#define ROLE_OWNER         "owner"
#define ROLE_LIBRARIAN     "librarian"

typedef struct user
{
     int id;
     char name[30];
     char email[30];
     char phone[12];
     char password[10];
     char role[10];
    
}user_t;

typedef struct book
 {
    int id;
    char name[10];
    char author[18];
    char subject[15];
    double price;
    char isdn[15];
 }book_t;

 typedef struct bookcopy
 {
     int id;
     int bookid;
     int rack;
     char status[13];
 }bookcopy_t;
    
typedef struct issuerecord
{
    int id;
    int copyid;
    int  memberId;
     date_t issuedate;
     date_t return_duedate;
     date_t returndate;
     double fine_amount;
}issuerecord_t;

typedef struct payment
{
    int id;
    int userid;
    double amount;
    char type[10];
    date_t transtime;
    date_t nextpayduedate;
}payment_t;


#endif

//user function
void user_accept(user_t *u);
void user_display(user_t *u);
void bookcopy_checkavail();


// book functions
void book_accept(book_t *b);
void book_display(book_t *b);


//owner
void owner_area(user_t *u);
void appoint_librarian();


//librarian
void add_member();
void librarian_area(user_t *u);
void book_edit_by_id();
void user_edit_by_id();
void bookcopy_add();
void bookcopy_checkavail_details();
int get_next_issuerecord_id();
void bookcopy_changestatus(int bookcopy_id, char status[]);

//member
void member_area(user_t *u);
void bookcopy_checkavail();

// common functions
void sign_in();
void sign_up();

void user_add(user_t *u);
int user_find_by_email(user_t *u, char email[]);
int get_next_user_id();



//book
void book_add(book_t *b);
void book_find_by_name(char name[]); 
int get_next_book_id();