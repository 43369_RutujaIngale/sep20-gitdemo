#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"
#include "comm.c"
#include "librarian.c"
#include "owner.c"
#include "member.c"
#include "date.h"
#include "date.c"


void tester()
{
    user_t u;
    book_t b;
    user_accept(&u);
    user_display(&u);
    book_accept(&b);
	book_display(&b);
}

void sign_in()
{
	char email[30], password[10];
	user_t u;
	int invalid_user = 0;
	// input email and password from user.
	printf("email: ");
	scanf("%s", email);
	printf("password: ");
	scanf("%s", password);
	// find the user in the users file by email.
	if(user_find_by_email(&u, email) == 1) {
		// check input password is correct.
		if(strcmp(password, u.password) == 0) {
			// special case: check for owner login
			if(strcmp(email, EMAIL_OWNER) == 0)
				strcpy(u.role, ROLE_OWNER);

			// if correct, call user_area() based on its role.
			if(strcmp(u.role, ROLE_OWNER) == 0)
				owner_area(&u);
			else if(strcmp(u.role, ROLE_LIBRARIAN) == 0)
				librarian_area(&u);
			else if(strcmp(u.role, ROLE_MEMBER) == 0)
				member_area(&u);
			else
				
				invalid_user = 1;
		}
		else
			//printf("Password Check fail.\n");
			invalid_user = 1;
	}
	else
		//printf("Email Check fail.\n");
		invalid_user = 1;

	if(invalid_user)
		printf("Invalid email, password or role.\n");

}



void sign_up()
{
	user_t u;
	//details from user
	user_accept(&u);
	//write user data into file
	user_add(&u);

}

void bookadd()
{
	book_t b;
	book_accept(&b);
	b.id=get_next_book_id();
	book_add(&b);
	
}



void book_find()
{
char name[80];
	printf("enter book name:");
	scanf("%s",&name);
	book_find_by_name(name);

}

int main(void)
{
  //tester();
   int choice;
	do {
		printf("\n\n0. Exit\n1. Sign In\n2. Sign Up\nEnter choice: ");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1:
			sign_in();
			break;
		case 2:
			sign_up();	
			break;
		/*case 3:
			bookadd();	
			break;
		case 4:
			book_find();	
			break;
		case 5:
			book_edit_by_id();	
			break;
		case 6:
			bookcopy_add();	
			break;	
		case 7:
			bookcopy_checkavail_details();
			break;
		case 8:
			bookcopy_checkavail();
			break;

		case 9:
			bookcopy_issue();
			break;

		case 10:
			bookcopy_return();
			break*/

			
		}
	}while(choice != 0);


    return 0;
}
